This is a simple Java application that determines the type of a triangle (equilateral, isosceles, or scalene) based on the lengths of its edges.

To build the application using Maven (and implicitly execute its unit tests), navigate to its root directory and execute the following command:

> mvn install

To run the application from the command line, navigate to its root directory and execute the following command:

> java -cp "./target/classes" com.vladacretoaie.triangles.Main

Note that the application expects three command line arguments representing integer edge lengths. The following invocation detects an equilateral triangle:

> java -cp "./target/classes" com.vladacretoaie.triangles.Main 1 1 1