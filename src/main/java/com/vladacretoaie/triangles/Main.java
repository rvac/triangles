package com.vladacretoaie.triangles;

/**
 * This class contains the main() method of the Triangle Types application.
 * 
 * @author Vlad Acretoaie
 * @version 1.0.0
 * @since 1.0.0
 */
public class Main {

	/**
	 * Determines the type of a triangle based on the three edge lengths provided as console arguments.
	 * 
	 * @param args console arguments
	 */
	public static void main(String[] args) {
		// Three arguments representing edge lengths are expected
		if (args.length != 3) {
			System.err.println("Please provide three integers representing edge lengths as arguments to this application.");
		} else {
			try {
				int[] edges = {Integer.parseInt(args[0]), Integer.parseInt(args[1]), Integer.parseInt(args[2])};
				switch (TriangleChecker.getTriangleKind(edges)) {
				case EQUILATERAL:
					System.out.println("The triangle is equilateral");
					break;
				case ISOSCELES:
					System.out.println("The triangle is isosceles");
					break;
				case SCALENE:
					System.out.println("The triangle is scalene");
					break;
				default:
					break;
				}
			} catch (NumberFormatException exception) {
				System.err.println("Please provide three integers representing edge lengths as arguments to this application.");
			} catch (IllegalArgumentException exception) {
				System.err.println(exception.getMessage());
			}
		}
	}

}
