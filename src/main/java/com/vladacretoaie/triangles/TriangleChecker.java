package com.vladacretoaie.triangles;

/**
 * This is a utility class operating on triangles.
 * 
 * @author Vlad Acretoaie
 * @version 1.0.0
 * @since 1.0.0
 */
public class TriangleChecker {
	
	/**
	 * Possible triangle kinds based on edge length.
	 */
	public static enum TriangleKind {
		EQUILATERAL, ISOSCELES, SCALENE
	}
	
	/**
	 * Returns the kind of a triangle based on its edge lengths.
	 * <br>
	 * A triangle with three equal edges is equilateral. A triangle with exactly two equal edges is isosceles. A triangle with no equal edges is scalene.  
	 * 
	 * @param edges the triangle edge lengths
	 * @return the triangle kind
	 * @throws IllegalArgumentException if the number of provided edge lengths is different than 3 or if at least one edge length is less than or equal to 0
	 */
	public static TriangleKind getTriangleKind(int[] edges) throws IllegalArgumentException {
		// Verify number of edges.
		if (edges.length != 3) {
			throw new IllegalArgumentException("A triangle must have exactly 3 edges.");
		}
		
		// Verify that all edge lengths are positive.
		for (int edge : edges) {
			if (edge <= 0) {
				throw new IllegalArgumentException("Edge lengths must be greater than 0.");
			}
		}
		
		// Check if the triangle is equilateral.
		if ((edges[0] == edges[1]) && (edges[1] == edges[2])) {
			return TriangleKind.EQUILATERAL;
		}
		
		// If the triangle is not equilateral, check if it is isosceles. 
		if ((edges[0] == edges[1]) || (edges[0] == edges[2]) || (edges[1] == edges[2])) {
			return TriangleKind.ISOSCELES;
		}
		
		// If the triangle is neither equilateral nor isosceles, it must be scalene.
		return TriangleKind.SCALENE;
	}

}