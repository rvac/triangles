package com.vladacretoaie.triangles;

import org.junit.Rule;
import org.junit.Test;
import org.junit.Assert;
import org.junit.rules.ExpectedException;

/**
 * This class contains unit tests for the TriangleChecker class.
 * 
 * @author Vlad Acretoaie
 * @version 1.0.0
 * @since 1.0.0
 */
public class TriangleCheckerTest {
	
	// Rule allowing the verification of thrown exceptions in test cases.
    @Rule
    public ExpectedException thrown = ExpectedException.none();
	
    // Test if an equilateral triangle is correctly detected.
	@Test
	public void testEquilateral() {
		int[] edges = {1,1,1};
		Assert.assertTrue(TriangleChecker.getTriangleKind(edges) == TriangleChecker.TriangleKind.EQUILATERAL);
	}
	
	// Test if an isosceles triangle is correctly detected.
	@Test
	public void testIsoscelesOne() {
		int[] edges = {1,1,2};
		Assert.assertTrue(TriangleChecker.getTriangleKind(edges) == TriangleChecker.TriangleKind.ISOSCELES);
	}
	
	// Test if an isosceles triangle is correctly detected.
	@Test
	public void testIsoscelesTwo() {
		int[] edges = {1,2,1};
		Assert.assertTrue(TriangleChecker.getTriangleKind(edges) == TriangleChecker.TriangleKind.ISOSCELES);
	}
	
	// Test if an isosceles triangle is correctly detected.
	@Test
	public void testIsoscelesThree() {
		int[] edges = {2,1,1};
		Assert.assertTrue(TriangleChecker.getTriangleKind(edges) == TriangleChecker.TriangleKind.ISOSCELES);
	}
	
	// Test if a scalene triangle is correctly detected.
	@Test
	public void testScalane() {
		int[] edges = {1,2,3};
		Assert.assertTrue(TriangleChecker.getTriangleKind(edges) == TriangleChecker.TriangleKind.SCALENE);
	}
	
	// Test if the correct exception is thrown when too many edges are provided.
	@Test
	public void testTooManyEdges() {
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("A triangle must have exactly 3 edges.");
		int[] edges = {1,2,3,4};
		TriangleChecker.getTriangleKind(edges);
	}

	// Test if the correct exception is thrown when too few edges are provided.
	@Test
	public void testTooFewEdges() {
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("A triangle must have exactly 3 edges.");
		int[] edges = {1,2};
		TriangleChecker.getTriangleKind(edges);
	}
	
	// Test if the correct exception is thrown when no edges are provided.
	@Test
	public void testZeroEdges() {
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("A triangle must have exactly 3 edges.");
		int[] edges = {};
		TriangleChecker.getTriangleKind(edges);
	}
	
	// Test if the correct exception is thrown when one edge has negative length.
	@Test
	public void testNegativeEdgeLength() {
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("Edge lengths must be greater than 0.");
		int[] edges = {1,2,-1};
		TriangleChecker.getTriangleKind(edges);
	}
	
	// Test if the correct exception is thrown when one edge has a length of 0.
	@Test
	public void testZeroEdgeLength() {
		thrown.expect(IllegalArgumentException.class);
		thrown.expectMessage("Edge lengths must be greater than 0.");
		int[] edges = {1,2,0};
		TriangleChecker.getTriangleKind(edges);
	}

}